#!/bin/bash

VERBOSE=0
NAME=""
IF=""
LOCAL_IP=""
GATEWAY_IP=""
DHCP_DYN=1
CHECK_DEVICES_OK=()
CHECK_DEVICES_NOK=()

STATS_NB_NOK=0

function check_if() {
    if [[ -n $(ip addr show $IF) ]]; then
        echo "[OK] Interface is $IF"
    else
        echo "[NOK] Interface is not $IF"
        ((STATS_NB_NOK++))
    fi
}

function check_ip() {
    if [[ -n $(ip addr show $IF | grep $LOCAL_IP) ]]; then
        echo "[OK] IP is $LOCAL_IP"
    else
        echo "[NOK] IP is not $LOCAL_IP"
        ((STATS_NB_NOK++))
    fi
}

function check_gateway() {
    if [[ -n $(ip route | grep $GATEWAY_IP) ]]; then
        echo "[OK] Gateway is settled to $GATEWAY_IP"
        if ping -c 1 "$GATEWAY_IP" > /dev/null; then
            echo "[OK] Gateway is reachable"
        else
            echo "[NOK] Gateway is not reachable"
            ((STATS_NB_NOK++))
        fi
    else
        echo "[NOK] Gateway is not settled to $GATEWAY_IP"
        ((STATS_NB_NOK++))
    fi
}

function check_dhcp() {
    if [[ "$VERBOSE" -eq 1 ]]; then
        echo "Suppression de l'ancienne négociation DHCP"
    fi
    sudo dhclient $IF -r
    if [[ "$VERBOSE" -eq 1 ]]; then
        echo "Nettoyage de l'interface réseau"
    fi
    sudo ip addr flush $IF
    if [[ "$VERBOSE" -eq 1 ]]; then
        echo "Attente de 5 secondes..."
    fi
    sleep 5
    if [[ "$VERBOSE" -eq 1 ]]; then
        echo "Lancement d'une négociation DHCP"
    fi
    RES=$(sudo timeout 10 dhclient $IF && echo "ok")
    if [[ -n "$RES" ]]; then
        echo "[OK] Négociation DHCP réussi"
    else
        echo "[NOK] Négaciation DHCP timeout"
        ((STATS_NB_NOK++))
    fi
    if [[ "$VERBOSE" -eq 1 ]]; then
        echo "Nettoyage de l'interface réseau"
    fi
    sudo ip addr flush $IF
}

function check_access_ok() {
    for i in "${CHECK_DEVICES_OK[@]}"; do
        if ping -c 1 "$i" > /dev/null; then
            echo "[OK] $i is reachable"
        else
            echo "[NOK] $i is not reachable"
            ((STATS_NB_NOK++))
        fi
    done
}

function check_access_nok() {
    for i in "${CHECK_DEVICES_NOK[@]}"; do
        if ping -c 1 "$i" > /dev/null; then
            echo "[NOK] $i is reachable"
            ((STATS_NB_NOK++))
        else
            echo "[OK] $i is not reachable"
        fi
    done
}

echo "Plan de test pour $NAME"
if [[ "$VERBOSE" -eq 1 ]]; then
    echo "Vérification de l'interface..."
fi
check_if
if [[ "$VERBOSE" -eq 1 ]]; then
    echo "Vérification du DHCP..."
fi
check_dhcp
if [[ "$DHCP_DYN" -ne 1 ]]; then
    if [[ "$VERBOSE" -eq 1 ]]; then
        echo "Vérification de l'IP..."
    fi
    check_ip
fi
if [[ "$VERBOSE" -eq 1 ]]; then
    echo "Vérification de la gateway..."
fi
check_gateway
if [[ "$VERBOSE" -eq 1 ]]; then
    echo "Vérification des accès autorisés"
fi
check_access_ok
if [[ "$VERBOSE" -eq 1 ]]; then
    echo "Vérification des accès non-autorisés"
fi
check_access_nok
echo "Nb errors: $STATS_NB_NOK"