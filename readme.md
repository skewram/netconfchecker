# NetConfChecker

Tool aiming to test network configuration on multiple device by generating for each one a bash script.

## Usage

* Fill access.yml
* Install pyyaml `pip install pyyaml`
* Run `python generator.py`
* Launch on each of your device it's generated shell script
