import yaml

with open("./template.sh", "r") as template:
    with open("./access.yml", "r") as conf_file:
        conf = yaml.load(conf_file, Loader=yaml.FullLoader)
        for v in conf['devices']:
            device = conf['devices'][v]
            VERBOSE = False
            NAME = device['name']
            IF = device['if']
            LOCAL_IP = device['ip']
            GATEWAY_IP = device['gateway']
            DHCP_DYN = device['dhcp_dynamic']
            CHECK_DEVICES_OK = device['rules']['ok']
            CHECK_DEVICES_NOK = device['rules']['nok']

            with open(f"./{v}.sh", "w+") as script:
                template_lines = template.readlines()
                wlines = []
                for tl in template_lines:
                    line = tl
                    if tl.startswith("VERBOSE="):
                        line = f"VERBOSE={'1' if VERBOSE else '0'}\n"
                    elif tl.startswith("NAME="):
                        line = f"NAME=\"{NAME}\"\n"
                    elif tl.startswith("IF="):
                        line = f"IF=\"{IF}\"\n"
                    elif tl.startswith("LOCAL_IP="):
                        line = f"LOCAL_IP=\"{LOCAL_IP}\"\n"
                    elif tl.startswith("GATEWAY_IP="):
                        line = f"GATEWAY_IP=\"{GATEWAY_IP}\"\n"
                    elif tl.startswith("DHCP_DYN="):
                        line = f"DHCP_DYN={'1' if DHCP_DYN else '0'}\n"
                    elif tl.startswith("CHECK_DEVICES_OK="):
                        line = "CHECK_DEVICES_OK=(" + ' '.join(
                            ['"'+d+'"' for d in CHECK_DEVICES_OK])+")\n"
                    elif tl.startswith("CHECK_DEVICES_NOK="):
                        line = "CHECK_DEVICES_NOK=(" + ' '.join(
                            ['"'+d+'"' for d in CHECK_DEVICES_NOK])+")\n"
                    wlines.append(line)
                script.writelines(wlines)
